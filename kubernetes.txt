Kubernetes is a portable, extensible, open-source platform for managing containerized workloads and services, that facilitates both declarative configuration and automation

The name Kubernetes originates from Greek, meaning helmsman or pilot. Google open-sourced the Kubernetes project in 2014. Kubernetes builds upon a decade and a half of experience that Google has with running production workloads at scale, combined with best-of-breed ideas and practices from the community.

Deployment:
TRADITIONAL 
Problema: risorse sotto-utilizzate o risorse contese tra app diverse. Difficile scalare.
==> VIRTUALIZED 
Problema: ridondanza (OS) e conseguente pesantezza
==> CONTAINERIZED


Orchestrazione
- health management (container restart)
- storage management
- secret and config management
- automation
- load balancing
- service discovery
- Disaccoppiamento SW - Hardware fisico --> maggior prevedibilità richiesta risorse (teorema fondamentale del limite)


Components' Architecture:
https://d33wubrfki0l68.cloudfront.net/7016517375d10c702489167e704dcb99e570df85/7bb53/images/docs/components-of-kubernetes.png
- kube-apiserver
- etcd (key-value store)
- kube-scheduler (prende decisioni su dove far girare i nuovi pod)
- kube-controller-manager
	- node-controller
	- replication-controller
	- endpoints-controller (joins Services and Pods)
	- service accounts & token controllers (Create default accounts and API access tokens for new namespaces)
- cloud-controller-manager


Node Components
- kubelet (The kubelet takes a set of PodSpecs that are provided through various mechanisms and ensures that the containers described in those PodSpecs are running and healthy. The kubelet doesn’t manage containers which were not created by Kubernetes.)
- kube-proxy (maintains network rules on nodes. These network rules allow network communication to your Pods from network sessions inside or outside of your cluster)
- Container Runtime (Docker, containerd, CRI-O, and any implementation of the Kubernetes CRI - Container Runtime Interface)


/api/v1

OpenAPI per spec

POD
A Pod is the basic execution unit of a Kubernetes application–the smallest and simplest unit in the Kubernetes object model that you create or deploy. A Pod represents processes running on your Cluster.


