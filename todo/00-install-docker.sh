# Steps
apt install -y apt-transport-https ca-certificates curl gnupg2 software-properties-common
# Install Docker from its official repository
curl -fsSL https://download.docker.com/linux/debian/gpg | apt-key add -
add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/debian
   stretch stable \
   stable"
apt update
apt install -y docker-ce
# Add vagrant user to docker group
usermod -G docker vagrant
