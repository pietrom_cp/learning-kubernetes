## Initialize master node, providing Flannel-specific network configuration schema
kubeadm init --pod-network-cidr=10.244.0.0/16 --apiserver-advertise-address=172.19.19.200 --token=anilre.soopersecrethere

kubeadm init --pod-network-cidr=10.244.0.0/16 --apiserver-advertise-address=172.29.3.10 --token=prismm.soopersecrethere

# --pod-network-cidr=192.168.0.0/16 # Calico
# 
# --pod-network-cidr=10.217.0.0/16 # Cilium

mkdir $HOME/.kube
cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
chown $(id -u):$(id -g) $HOME/.kube/config
## Install Flannel
# sudo sysctl net.bridge.bridge-nf-call-iptables=1

# Flanell
# - --iface=enp0s8
# --pod-network-cidr=10.244.0.0/16 # Flanell
# https://raw.githubusercontent.com/coreos/flannel/2140ac876ef134e0ed5af15c65e414cf26827915/Documentation/kube-flannel.yml
kubectl apply -f /vagrant/descriptors/kube-flannel.yml

# Calico
kubectl apply -f https://docs.projectcalico.org/manifests/calico.yaml

# Weave
kubectl apply -f "https://cloud.weave.works/k8s/net?k8s-version=$(kubectl version | base64 | tr -d '\n')"

# If master should act as a worker
kubectl taint nodes --all node-role.kubernetes.io/master-

## Verify cluster status 
kubectl get nodes







