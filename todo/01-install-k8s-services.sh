# Install Kubernetes packages from their official repository
curl -fsSL https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -
add-apt-repository \
   "deb [arch=amd64] http://apt.kubernetes.io
   kubernetes-xenial \
   main"
apt update
apt install -y kubelet kubeadm kubectl
