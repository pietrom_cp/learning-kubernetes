#!/bin/bash
if [ $# -ne 2 -a $# -ne 1 ] ; then
	echo "Usage: config.sh CLUSTER_HOSTS_FILE [CLUSTER_NODE_NAME]"
	exit 1
fi

nodeName="${2}"
if [ -z "${nodeName}" ] ; then
	nodeName=$(hostname)
fi
nodeIp=$(cat ${1} | grep ${nodeName} |tr -s '' | cut -d ' ' -f 1)

echo "Configuring node ${nodeName} with IP ${nodeIp}"

iptables -t nat -F
iptables -F 
iptables -X
ip link set docker0 down
ip link delete docker0
cat $1 >> /etc/hosts
sed -ie "s/^ExecStart=\$/ExecStart=\\
Environment=\"KUBELET_EXTRA_ARGS=--node-ip=${nodeIp}\"/" /etc/systemd/system/kubelet.service.d/10-kubeadm.conf
systemctl daemon-reload
systemctl restart kubelet.service